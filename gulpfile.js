const bodyParser = require('body-parser');
const { response } = require('express');
const { join } = require('path');

let gulp             = require('gulp'),
    chalk            = require('chalk'),
    express          = require('express'),
    app              = express(),
    path             = require('path'),
    mongoose         = require('mongoose'),
    {config, engine} = require('express-edge'),
    session          = require('express-session'),
    flash            = require('connect-flash'),
    cookieParser     = require('cookie-parser'),
    multer           = require('multer'),
    storage          = multer.diskStorage({
                        destination: 'libros/',
                        filename: function(req, file, cb){
                            if (file.originalname.slice(-3) == 'pdf' || file.originalname.slice(-3) == 'PDF'){
                                cb("",file.originalname );
                            }
                            else{console.log(chalk.bgGreenBright(file.originalname));}
                        }}),
    upload           = multer({storage:storage}),
    passport         = require('passport'),
    url              = require('url'),
    download         = require('download-pdf'),
    Escritor         = require('./Database/Models/Escritor'),
    Libro            = require('./Database/Models/Libro'),
    Admin            = require('./Database/Models/Admin');


//---------------------------------------------------------------
mongoose.connect('mongodb://localhost/mundo_libros');
//require('./config/passportEscritor')(passport);

//require('./config/passport')(passport);
const sesionescritor = require('./config/passportEscritor');
const sesionAdmin = require('./config/passport');
//---------------------------------------------------------------------
app.use(engine);
app.use(express.static(path.join(__dirname,'/views')));
app.set('views',`${__dirname}/views`);
app.use(cookieParser());
app.use(session({
    secret: 'SeCreTo',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
//app.use(passport2.initialize());
//app.use(passport2.session());
//-------------------------------------------------------------------------
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));
app.use(flash());
//-------------------------------------------------------------------------
// ------- Creacion Cuenta Admin si no existe -----------------------------
Admin.findOne({'email':"admin@mundolibros.com"}, (error, administrador)=> {
    if(error) {}
    if(!administrador)
    {
        let admin = new Admin();
        admin.email  = "admin@mundolibros.com";
        admin.contrasenia = admin.generateHash("admin123");
        admin.save();
    }
})
// ----------------------------------------------------------------------

gulp.task('server', ()=>{
    app.listen(4001);
    console.log(chalk.redBright('*****   Servidor corriendo   *******'));
    //console.log(chalk.redBright('*****   watch activo   *******'));
    //gulp.watch(['views/*.edge','views/*.css','./*.js'],gulp.series('server'));
    //bw({ proxy: 'localhost:4001'});
    //done();
});
//-------------- Comprobar inicio de sesion ----------------
//function sesionActiva(req, res, next){
//    if (req.isAuthenticated()) { return next();}
//    return res.redirect('/');
//}
//---------------  llamados a paginas reenderizadas ---------------
app.get('/', (reques, response) => { response.render('home');});
// -------------------- Escritor -----------------------------------------------------
app.get('/formLoginEscritor', (reques, response) => {
    response.render('formLoginEscritor',{ message: reques.flash('loginEscMessage')});
});

app.get('/formEscritor', (reques, response) => {
    response.render('formEscritor',{ message: reques.flash('signupMesassge')});
});

//app.get('/formLibro', sesionActiva, (reques, response) => {
app.get('/formLibro', sesionescritor.escritorAutenticado, (reques, response) => {
    response.render('formLibro', {escritor:reques.user})
});

//app.get('/homeEscritor', sesionActiva, (reques, response) => {
app.get('/homeEscritor',sesionescritor.escritorAutenticado, (reques, response) => {
    response.render('homeEscritor', {escritor:reques.user});
});

//app.get('/verLibrosEscritor', sesionActiva, async (reques, response) =>{
app.get('/verLibrosEscritor', sesionescritor.escritorAutenticado, async(reques, response) => {
    let libros = await Libro.find({'autor':reques.user.email});
    response.render('verLibrosEscritor',{libros});
})

//app.get('/formEditarEscritor', sesionActiva, (reques, response)=>{
app.get('/formEditarEscritor', sesionescritor.escritorAutenticado, (reques, response) => {
    response.render('formEditarEscritor', {escritor:reques.user});//, message:reques.flash('errorMsg')});
})

app.get('/cerrarSesion', (req, res)=>{
    req.logout();
    res.redirect('/');
});

// ----------------------------------------------------------
// ------------- Administrador ------------------------------
app.get('/formLoginAdmin', (reques, response) => {
    response.render('formLoginAdmin',{ message: reques.flash('loginAdminMessage')});
});

app.get('/homeAdmin', sesionAdmin.adminAutenticado, (reques, response) => {
    response.render('homeAdmin');
});

app.get('/verEscritor', sesionAdmin.adminAutenticado, async (reques,response) =>{
    let escritores = await Escritor.find({});
    response.render('verEscritor',{escritores});
});

app.get('/verLibrosAdmin', sesionAdmin.adminAutenticado, async(reques, response) => {
    let libros = await Libro.find({});
    response.render('verLibrosEscritorcopia',{libros});
})
//-----------------------------------------------------------

// ------------------ generales -------------------------------
app.get('/homeLibros', async(reques, response) => {
    let libros = await Libro.find({});
    response.render('homeLibros',{libros});
})

app.get('/descargarPdf', (reques, response) => {
    let Get_param = url.parse(reques.url,true).query;
    var pdf = Get_param.ruta;

    //let count_d = Get_param.descarga;
    response.download(pdf,function(error){
            if(error){
                console.log(error);
            }
            else 
            { 
                console.log("Listo");
                dwycf(Get_param.codigo,"dw");
            } 
        })
})

app.get('/megustaLibro', (reques, response) => {
    let Get_param = url.parse(reques.url,true).query;
    var pdf = Get_param.ruta;
    dwycf(Get_param.codigo,"cfb");
    response.redirect('/homeLibros');

})

app.get('/nomegustaLibro', (reques, response) => {
    let Get_param = url.parse(reques.url,true).query;
    var pdf = Get_param.ruta;
    dwycf(Get_param.codigo,"cfm");
    response.redirect('/homeLibros');
})
// ----------------------------------------------------------

// ------------ consultas en bases ---------------------------
app.post('/eliminar/libro', (reques, response) => {
    const idLibro = reques.body.idLibro,
          usuario = reques.body.usuario;
        
    Libro.findByIdAndRemove(idLibro, (error) => {
        if(error) { response.send(error);}
        else      
        { 
            if (usuario=="escritor") {response.redirect('/verLibrosEscritor');}
            if (usuario=="admin") {response.redirect('/verLibrosAdmin');}
        }
    });
});


app.post('/eliminar/libro/admin', (reques, response) => {
    const idLibro = reques.body.idLibro,
          usuario = reques.body.usuario;
        
    Libro.findByIdAndRemove(idLibro, (error) => {
        if(error) { response.send(error);}
        else      
        { 
            if (usuario=="escritor") {response.redirect('/verLibrosEscritor');}
            if (usuario=="admin") {response.redirect('/verLibrosAdmin');}
        }
    });
});
// ------------ Administrador ----------------------------------
app.post('/formLoginAdmin', passport.authenticate('localAdmin-login',{
    successRedirect: '/homeAdmin',
    failureRedirect: '/formLoginAdmin',
    failureFlash: true
}));

app.post('/eliminarEscritor', (reques, response) => {
    const idEscritor = reques.body.idEscritor;    
    Escritor.findByIdAndRemove(idEscritor, (error) => {
        if(error) { response.send(error);}
        else      { response.redirect('/verEscritor');}
    });
});
// -----------------------------------------------------------
// -------------- Escritor -----------------------------------
app.post('/formEscritor',passport.authenticate('local-signup', {
    successRedirect: '/homeEscritor',
    failureRedirect: '/formEscritor',
    failureFlash: true
}));

app.post('/formLoginEscritor', passport.authenticate('local-login',{
    successRedirect: '/homeEscritor',
    failureRedirect: '/formLoginEscritor',
    failureFlash: true
}));

//app.post('/registrar/libro',sesionActiva,upload.single('filePdf'),  (reques, response) =>{
app.post('/registrar/libro',sesionescritor.escritorAutenticado,upload.single('filePdf'),  (reques, response) =>{
    let tit = reques.body.titulo,
        cat = reques.body.categoria,
        des = reques.body.descripcion;
    const    aut = reques.body.autor;
    var cod=000;
    cod = cod+1;
    let nuevolibro = new Libro();
    nuevolibro.codigo      = path.join(path.join("mdl-",cod.toString()),(reques.file.originalname).slice(0,-4));
    nuevolibro.autor       = aut;
    nuevolibro.titulo      = tit;
    nuevolibro.categoria   = cat;
    nuevolibro.descarga    = 0;
    nuevolibro.calificacion= 0;
    nuevolibro.descripcion = des;
    nuevolibro.rutaDesc    = path.join(path.join(__dirname,'/libros'),reques.file.originalname);
    nuevolibro.save(function(err){
        if(err) {throw err;}
    });
    response.redirect('/homeEscritor');
});

//app.post('/formEditarEscritor',sesionActiva, (reques, response) => {
app.post('/formEditarEscritor',sesionescritor.escritorAutenticado, (reques, response) => {
    const idEscritor = reques.body.idEscritor;
    var flag=0;
    var pwecryp="";

    Escritor.findOne({'_id':idEscritor},function (err,escritor){
        
        pwecryp=escritor.generateHash(reques.body.nueva_contrasenia);
        cambiar();

    });
    function cambiar()
    {
        Escritor.findByIdAndUpdate(idEscritor,{
            contrasenia: pwecryp
        },function (err, esc) { response.redirect('/homeEscritor')})
    }
    
});
// ----------------------------------------------------------------
function dwycf(id, dato)
{
    var aumento =0;
    Libro.findOne({'_id':id},function (err,lb){

        if(dato=="dw") {
            console.log(chalk.redBright(lb.descarga));
            aumento= lb.descarga;
            aumento = aumento+1;
            console.log(chalk.redBright(aumento));
            Libro.findByIdAndUpdate(id,{
                descarga: aumento
            },function (err, esc) {})
        }
        if(dato=="cfb") {
            aumento= lb.calificacion;
            aumento = aumento+1;
            Libro.findByIdAndUpdate(id,{
                calificacion: aumento
            },function (err, esc) {})
        }
        if(dato=="cfm") {
            aumento= lb.calificacion;
            aumento = aumento-1;
            Libro.findByIdAndUpdate(id,{
                calificacion: aumento
            },function (err, esc) {})
        }
        return;
    });
}

gulp.task('default',gulp.series('server'),()=>{});