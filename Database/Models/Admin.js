const mongoose = require('mongoose');
const bcrypt   = require('bcrypt-nodejs')

const AdminSchema = new mongoose.Schema({
    email :      String,
    contrasenia: String 
});

AdminSchema.methods.generateHash = function(contrasenia){
    return bcrypt.hashSync(contrasenia, bcrypt.genSaltSync(8),null);
};

AdminSchema.methods.validarContrasenia = function(contrasenia){
    return bcrypt.compareSync(contrasenia, this.contrasenia);
};

const Admin = mongoose.model('Admin',AdminSchema);
module.exports = Admin;