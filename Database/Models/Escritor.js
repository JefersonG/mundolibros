const mongoose = require('mongoose');
const bcrypt   = require('bcrypt-nodejs')

const EscritorSchema = new mongoose.Schema({
    email :      {type: String, unique:true, lowercase:true},
    cedula:      String,
    nombre:      String,
    apellido:    String,
    contrasenia: String 
});
EscritorSchema.methods.generateHash = function(contrasenia){
    return bcrypt.hashSync(contrasenia, bcrypt.genSaltSync(8),null);
};
EscritorSchema.methods.validarContrasenia = function(contrasenia){
    return bcrypt.compareSync(contrasenia, this.contrasenia);
};
const Escritor = mongoose.model('Escritor',EscritorSchema);
module.exports = Escritor; 