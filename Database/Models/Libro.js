const mongoose = require('mongoose');
//const Escritor = mongoose.model('Escritor');

const LibroSchema = new mongoose.Schema({
    codigo:         String,
    titulo:         String,
    categoria:      String,
    descarga:       Number, //cambiara a entero
    calificacion:   Number, // ca,biar a entero
    rutaDesc:       String, //ruta del almacenamiento de pdf
    descripcion:    String,
    autor:          String
});

const Libro = mongoose.model('Libro',LibroSchema);
module.exports = Libro;