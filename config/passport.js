const chalk = require('chalk');

const localS   = require('passport-local').Strategy,
      passport = require('passport'),
      Admin    = require('../Database/Models/Admin');



/*
module.exports = (passport) =>{
    passport.serializeUser((admin, done) =>{
        done(null, admin.id);
    });
    
    passport.deserializeUser((id, done) => {
        Admin.findById(id, (err,admin)=>{
            done(err,admin);
        });
    });

    //--------------------- logearse como admin ---------------------------------------------------
    passport.use('localAdmin-login',new localS({
        usernameField: 'email',
        passwordField: 'contrasenia',
        passReqToCallback: true
    },function (req, email, contrasenia, done){
        Admin.findOne({'email':email}, function (err,admin){
            if(err) {return done(err);}
            if(!admin)
            {
                return done(null, false, req.flash('loginAdminMessage','Este correo no corresponde al administrador'));
            }
             
            if (!admin.validarContrasenia(contrasenia)) 
            { 
                return done(null, false, req.flash('loginAdminMessage','contraseña incorrecta'));
            }
            return done(null, admin);
        });
    }));
}

*/

passport.serializeUser((user, done) =>{
    console.log(chalk.greenBright("serialiar admin"));
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    Admin.findById(id, (err,user)=>{
        console.log(chalk.redBright("desserialiar admin"));
        done(err,user)
    });
});


passport.use('localAdmin-login',new localS({
    usernameField: 'email',
    passwordField: 'contrasenia',
    passReqToCallback: true
},function (req, email, contrasenia, done){
    Admin.findOne({'email':email}, function (err,admin){
        if(err) {return done(err);}
        if(!admin)
        {
            return done(null, false, req.flash('loginAdminMessage','Este correo no corresponde al administrador'));
        }
         
        if (!admin.validarContrasenia(contrasenia)) 
        { 
            return done(null, false, req.flash('loginAdminMessage','contraseña incorrecta'));
        }
        return done(null, admin);
    });
}));

exports.adminAutenticado = (req, res, next) =>{
    if (req.isAuthenticated()) {
        console.log(chalk.greenBright("esta autenticando Admin"));
        return next();
    }
    console.log(chalk.greenBright("no autentico admin"));
    return res.redirect('/');

    //res.status(401).send('Tienes que ser adminitrador para acceder');
}