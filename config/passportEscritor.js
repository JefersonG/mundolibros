const chalk = require('chalk');

const localS   = require('passport-local').Strategy,
      passport = require('passport'),
      Escritor    = require('../Database/Models/Escritor');




passport.serializeUser((user, done) =>{
    console.log(chalk.greenBright("serialiar Escritor"));
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    Escritor.findById(id, (err,user)=>{
        console.log(chalk.redBright("desserialiar Escritor"));
        done(err,user)
    });
});


//--------------------- crear sesión ---------------------------------------------------
passport.use('local-signup',new localS({
    usernameField: 'email',
    passwordField: 'contrasenia',
    passReqToCallback: true
},function(req, email, contrasenia, done){
    Escritor.findOne({'email':email}, function (err,escritor){
        if(err) {return done(err);}
        if(escritor)
        {
            return done(null, false, req.flash('signupMesassge','Ya existe un escritor con ese email'));
        }
        else 
        {
            var nuevoEscritor = new Escritor();
            nuevoEscritor.email  = email;
            nuevoEscritor.cedula = req.body.cedula;
            nuevoEscritor.nombre = req.body.nombre;
            nuevoEscritor.apellido = req.body.apellido;
            nuevoEscritor.contrasenia = nuevoEscritor.generateHash(contrasenia);
            nuevoEscritor.save(function(err) {
                if(err) {
                    throw err;
                }
                return done(null, nuevoEscritor);
            }); 
        }
    });
}));

//--------------------- logearse como escritor ---------------------------------------------------
passport.use('local-login',new localS({
    usernameField: 'email',
    passwordField: 'contrasenia',
    passReqToCallback: true
},function (req, email, contrasenia, done){
    Escritor.findOne({'email':email}, function(err,escritor){
        if(err) {return done(err);}
        if(!escritor)
        {
            return done(null, false, req.flash('loginEscMessage','No hay un escritor registardo con ese correo'));
        }
         
        if (!escritor.validarContrasenia(contrasenia)) 
        { 
            return done(null, false, req.flash('loginEscMessage','contraseña incorrecta'));
        }
        return done(null, escritor);
    });
}));

exports.escritorAutenticado = (req, res, next) =>{
    if (req.isAuthenticated()) {
        console.log(chalk.greenBright("esta autenticando escritor"));
        return next();
    }
    console.log(chalk.greenBright("no autentico escritor"));
    return res.redirect('/');

    //res.status(401).send('Tienes que ser adminitrador para acceder');
}




/*const localS   = require('passport-local').Strategy,
      Escritor = require('../Database/Models/Escritor'),
      Admin    = require('../Database/Models/Admin'),
      chalk    = require('chalk');




module.exports = (passport) =>{
    passport.serializeUser((user, done) =>{
        console.log(chalk.greenBright("serialiar admin"));
        console.log(chalk.greenBright(user));
        done(null, user.id);
    });
    
    passport.deserializeUser((id, done) => {
        Escritor.findById(id, (err,escritor)=>{
            done(err,escritor)
        });
    });

    //--------------------- crear sesión ---------------------------------------------------
    passport.use('local-signup',new localS({
        usernameField: 'email',
        passwordField: 'contrasenia',
        passReqToCallback: true
    },function(req, email, contrasenia, done){
        Escritor.findOne({'email':email}, function (err,escritor){
            if(err) {return done(err);}
            if(escritor)
            {
                return done(null, false, req.flash('signupMesassge','Ya existe un escritor con ese email'));
            }
            else 
            {
                var nuevoEscritor = new Escritor();
                nuevoEscritor.email  = email;
                nuevoEscritor.cedula = req.body.cedula;
                nuevoEscritor.nombre = req.body.nombre;
                nuevoEscritor.apellido = req.body.apellido;
                nuevoEscritor.contrasenia = nuevoEscritor.generateHash(contrasenia);
                nuevoEscritor.save(function(err) {
                    if(err) {
                        throw err;
                    }
                    return done(null, nuevoEscritor);
                }); 
            }
        });
    }));

    //--------------------- logearse como escritor ---------------------------------------------------
    passport.use('local-login',new localS({
        usernameField: 'email',
        passwordField: 'contrasenia',
        passReqToCallback: true
    },function (req, email, contrasenia, done){
        Escritor.findOne({'email':email}, function(err,escritor){
            if(err) {return done(err);}
            if(!escritor)
            {
                return done(null, false, req.flash('loginEscMessage','No hay un escritor registardo con ese correo'));
            }
             
            if (!escritor.validarContrasenia(contrasenia)) 
            { 
                return done(null, false, req.flash('loginEscMessage','contraseña incorrecta'));
            }
            return done(null, escritor);
        });
    }));
    //--------------------- logearse como admin ---------------------------------------------------
    /*passport.use('localAdmin-login',new localS({
        usernameField: 'email',
        passwordField: 'contrasenia',
        passReqToCallback: true
    },function (req, email, contrasenia, done){
        Admin.findOne({'email':email}, function (err,admin){
            if(err) {return done(err);}
            if(!admin)
            {
                return done(null, false, req.flash('loginAdminMessage','Este correo no corresponde al administrador'));
            }
             
            if (!admin.validarContrasenia(contrasenia)) 
            { 
                return done(null, false, req.flash('loginAdminMessage','contraseña incorrecta'));
            }
            return done(null, admin);
        });
    }));
}*/